# Introduction

Solutions to the challenges of reto-devops from CLM, using `minikube`.

## Requisites

You should have installed the following packages in your machine, in order for the `Makefile` commands to work:

* openssl
* virtualbox
* docker
* docker-compose
* helm
* kubectl
* terraform
* minikube (although you should be able to use another k8s cluster manager)


## Usage

For ease of use the `Makefile` contains every *stage* necessary to test the solutions. Nonetheless I'll walk you through the commands to further validate every challenge.

1. Start `minikube`

   ```bash
    minikube --memory 4096 --cpus 2 start --vm-driver=virtualbox
   ```
2. Start the `docker-compose` solution

   ```bash
   make docker
   ```

2. Check that the `/private` path is protected. You should get a `401 Authorization Required`. Use the `-u 'admin:admin'` flag to get authenticated.

   ```bash
   curl -kL https://localhost/private 
   ```
3. Clean the `docker-compose` images to continue

   ```bash
   make docker-d
   ```
4. Deploy the `k8s` solution: deployment, service & hpa.

   ```bash
   make k8s
   ```
 
5. On a another `shell` start this process to access the `service` (may be `minikube` related only). Keep it running.

   ```bash
   minikube tunnel 
   ```
 
6. Hit the `/` path through the `service`. You should get a `{"msg":"ApiRest prueba"}`

   ```bash
    curl $(kubectl get svc | awk '/node-clm/{ print $3 }'):3000 
   ```
 
7. Clean the k8s resources. You can now end the `minikube tunnel` process.

   ```bash
   make k8s-d
   ```
 
8. Install the helm charts to test the 5th challenge. This uses an `nginx ingress controller` with `ingress rules` for basic authentication, on the `clm-node namespace`.

   ```bash
   make helm
   ```
 
9. Hit the `/private` endpoint through the `cluster ip`. You should get a `401 Authorization Required`. Use the `-u 'admin:admin'` flag to get authenticated.

   ```bash
   curl http://$(minikube ip)/private -H 'Host: clm-node.info'
   ```
10. Switch to the `clm-node namespace` and list its resources.

   ```bash
    kubectl config set-context --current --namespace=clm-node && kubectl get all
   ```
11. Create a `k8 user` to test the 6th challenge `RBAC`. After running this command try to `kubectl get pods`, you should get a `Forbidden` error.

   ```bash
    make k8s-user
   ```
12. Deploy the 6th solution with the custom `rbac_role_read_only` module.

   ```bash
    make terraform
   ```
13. We should now be able to list the `pods`.

   ```bash
    kubectl get pods
   ```
14. Finally clean your workspace.

   ```bash
    make terraform-d helm-d && minikube delete
   ```
## Notes

The .gitlab-ci.yaml:
* tests the app
* builds the app & nginx image
* pushes the images to a public ECR repository `public.ecr.aws/w1l3p6y1`

## License
[MIT](https://choosealicense.com/licenses/mit/)
