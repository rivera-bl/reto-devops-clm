module "rbac_role_read_only" {
  source = "./modules/rbac_role_read_only"

  config_path    = "~/.kube/config"
  config_context = "minikube"
  namespace      = "clm-node"

  prefix_role_name = "clm"

  k8_resources = ["pods"]
  user_name    = "user1"
}
