provider "kubernetes" {
  config_path    = var.config_path
  config_context = var.config_context
}

resource "kubernetes_role" "this" {
  metadata {
    name      = "${var.prefix_role_name}-role"
    namespace = var.namespace
  }

  rule {
    api_groups = ["*"]
    resources  = var.k8_resources
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_role_binding" "this" {
  metadata {
    name      = "${var.prefix_role_name}-role-binding"
    namespace = var.namespace
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "${var.prefix_role_name}-role"
  }

  subject {
    kind      = "User"
    name      = var.user_name
    api_group = "rbac.authorization.k8s.io"
  }
}
