variable "config_context" {
  description = "Config context"
  type        = string
}

variable "config_path" {
  description = "Config Path"
  type        = string
}

variable "namespace" {
  description = "Namespace for the Role & Role Binding"
  type        = string
}

variable "prefix_role_name" {
  description = "Prefix name to user for the Role & Role Binding"
  type        = string
}

variable "k8_resources" {
  description = "K8 Resources to allow"
  type        = list(string)
}

variable "user_name" {
  description = "User to which bind the Role"
}
