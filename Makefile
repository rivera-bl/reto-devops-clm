.PHONY: docker
docker: 
	@docker-compose up --build -d

.PHONY: k8s
k8s:
	@kubectl apply -f 4-k8s/node-deploy.yml
	@kubectl apply -f 4-k8s/node-service.yml
	@kubectl apply -f 4-k8s/node-hpa.yml

.PHONY: helm
helm:
	@minikube addons enable ingress
	@helm install clm-node 5-helm

.PHONY: k8s-user
k8s-user:
	@openssl genrsa -out /tmp/user1.key 2048
	@openssl req -new -key /tmp/user1.key -out /tmp/user1.csr -subj "/CN=user1/O=group1"
	@openssl x509 -req -in /tmp/user1.csr -CA ~/.minikube/ca.crt -CAkey ~/.minikube/ca.key -CAcreateserial -out /tmp/user1.crt -days 500
	@kubectl config set-credentials user1 --client-certificate=/tmp/user1.crt --client-key=/tmp/user1.key
	@kubectl config set-context user1-context --user=user1
	@kubectl config use-context user1-context

.PHONY: terraform
terraform:
	@cd 6-terraform && terraform init
	@cd 6-terraform && terraform apply --auto-approve

# CLEAN targets
.PHONY: docker-d
docker-d: 
	@docker-compose down --rmi all

.PHONY: k8s-d
k8s-d:
	@kubectl delete -f 4-k8s/node-deploy.yml
	@kubectl delete -f 4-k8s/node-hpa.yml
	@kubectl delete -f 4-k8s/node-service.yml

.PHONY: terraform-d
terraform-d:
	@cd 6-terraform && terraform destroy --auto-approve

.PHONY: helm-d
helm-d:
	@kubectl config use-context minikube
	@helm delete clm-node
